const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const app = express();
const PORT = 8080;
const morgan = require('morgan');
const { notesRouter } = require('./controllers/notesController');
const { authRouter } = require('./controllers/authController');
const { userRouter } = require('./controllers/userController');
const { authMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);

app.use(authMiddleware);
app.use('/api/notes', notesRouter);

app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found' })
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://TaniaBond:m0ng0Pa$$@cluster0.dghps.mongodb.net/test',
            {
                useNewUrlParser: true, useUnifiedTopology: true
            });
        app.listen(PORT);
    } catch (err) {
        console.error(`Error on server start: ${err.message}`);
    }
    
}

start();
