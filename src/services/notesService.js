const { Note } = require('../models/noteModel');

const getNotesByUserId = async (userId) => {
    const notes=  await Note.find({userId})
    return notes;
}

const addNoteToUser = async ( userId, notePayload ) => {
    const note = new Note({ ...notePayload, userId });
    await note.save();
}

const getNoteById = async (noteId) => {
    const note = await Note.findById( noteId )
    return note;
}

const updateNoteById = async (noteId, userId, noteText) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: {text: noteText}});
};

const deleteNoteById = async (noteId) => {
  await Note.findOneAndRemove({_id: noteId});
};

const changeStatus = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  await note.updateOne({$set: {completed: !note.completed}});
};


module.exports = {
    getNotesByUserId,
    addNoteToUser,
    getNoteById,
    updateNoteById,
    changeStatus,
    deleteNoteById
}