const mongoose = require('mongoose');

const Note = mongoose.model('Notes',
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,

        },
        text: {
            type: String,
            unique: true,
            required: true
        },
        createdDate: {
            type: Date,
            default: Date.now()
        },
        completed: {
            type: Boolean,
            default: false
        }
    });

module.exports = { Note };
