const express = require('express');
const router = express.Router();

const { User } = require('../models/userModel');

router.get('/:id', async (req, res) => {
    const { userId } = req.params.id;
    try {
        const user = await User.findOne({userId},);
        res.json({
            "user": {
                "_id": user._id,
                "username": user.username,
                "createdDate": user.createdDate
            }
        });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.patch('/:id', async (req, res) => {
    const { userId } = req.params.id;
    const data = req.body;
   
        const user = await User.findByIdAndUpdate({ userId }, { $set: data });
        res.json({ message: 'Success' });
    
});

router.delete('/:id', async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const user = await User.findOneAndRemove({_id: me, userId});
    res.json({message: 'Success'});
});


module.exports = {
    userRouter:router
}

