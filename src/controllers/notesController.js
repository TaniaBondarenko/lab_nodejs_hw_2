const express = require('express');
const router = express.Router();
const {getNotesByUserId, addNoteToUser, getNoteById, updateNoteById, changeStatus, deleteNoteById} = require('../services/notesService')

router.get('/', async (req, res) => {
    const { userId } = req.user;
    try {
        const notes = await getNotesByUserId(userId);
       res.json({ notes });
   } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.post('/', async (req, res) => {
    const { userId } = req.user;
    try {
        await addNoteToUser(userId, req.body);
        res.json({ message: 'Success' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const note = await getNoteById(id);
        res.json({ note });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.put('/:id', async (req, res) => {
    const { text } = req.body;
    try {
        await updateNoteById(text);
        res.json({ message: "Success" });
     } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        await deleteNoteById(id);

        res.json({message: "Success" });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.patch('/:id', async (req, res) => {
    const {userId} = req.user;
  const {id} = req.params;

  await changetatus(id, userId);

  res.json({message: 'Success'});

});

module.exports = {
    notesRouter:router
}
