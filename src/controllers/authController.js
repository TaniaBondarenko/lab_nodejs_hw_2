const express = require('express');
const router = express.Router();

const { register, logIn } = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;
        await register({ username, password });
       
        res.json({ message: 'Success' })
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.post('/login', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;
        const token = await logIn({ username, password });
        res.json({message:'Success', token})
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
    
});

module.exports = {
    authRouter:router
}
